# 使用Selenium & Chrome 進行環境設定

## 前置動作
1. 安裝Node.js
1. 安裝Chrome

## 啟動方式
### 執行以下指令
```
npm install
npm run config
```

## 環境說明
環境設定參數置於`test_data.yml`檔案中，系統會使用`git_configuration.js`進行**git**環境的初始設定

## 問題排解
- 如果無法正確載入Chromedriver，請手動上 [ChromeDriver](http://chromedriver.chromium.org/downloads)下載並解壓縮，<font color=red>需注意下載版本須與電腦裡的Chrome是一致的(例如:Chrome 75.xxx的只能下載 75系列的Driver)</font>