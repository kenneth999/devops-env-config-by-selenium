/**
 * ref https://examples.javacodegeeks.com/enterprise-java/selenium/selenium-nodejs-example/
 */

require('chromedriver');
const {
    Builder,
    By,
    Key,
    until
} = require('selenium-webdriver');
const yaml = require('js-yaml');
const fs = require('fs');

var stepCounter = 1;
var driver;
var param = yaml.safeLoad(fs.readFileSync('test_data.yml', 'utf8'));
console.log('test_data.yml data: ');
console.log(param);

var sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

var beforeEach = () => {
    driver = new Builder()
        .forBrowser('chrome')
        .build();
};

var afterEach = () => {
    driver.quit();
};

var click = async (by) => {
    console.log(`${stepCounter} click ${by}`)
    stepCounter++;

    var element = await driver.findElement(by);
    
    await driver.wait(until.elementIsVisible(element), param.git.waitingTime);
    await element.click();
}

var input = async (by, data) => {
    console.log(`${stepCounter} input ${data} into ${by}`)
    stepCounter++;

    var element = await driver.findElement(by);
    await driver.wait(until.elementIsVisible(element), param.git.waitingTime);
    await element.sendKeys(data, Key.NULL);
    //await element.sendKeys(data, Key.RETURN);
}

var screen = async (fileName) => {
    console.log(`${stepCounter} takeScreen ${fileName}`)

    var dir = 'screen';
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    let image = await driver.takeScreenshot();
    fs.writeFileSync(`${dir}/${stepCounter}_${fileName}.png`, image, {'encoding':'base64', "flag":'w'});

}

var openWebsite = async () => {
    await driver.get(param.git.url);
}

var changeLocale = async () => {
    await click(By.css(".language"));
    await sleep(500);
    await click(By.linkText("English"));
    await sleep(500);
}

var login = async () => {
    await click(By.linkText("Sign In"));

    await input(By.id("user_name"), param.git.account.name);
    
    await input(By.id("password"), param.git.account.passwd);

    await screen("login");

    await click(By.xpath("//button[contains(text(), 'Sign In')]"));
}

var addRemoteRepository = async () => {
    await click(By.xpath("//div[@data-content='Create…']"));

    //'遷移外部儲存庫'
    await sleep(500);
    await click(By.xpath("//a[@href='/repo/migrate']"));

    await input(By.id("clone_addr"), param.git.clone['remote-repo-url']);

    await screen("addRemoteRepository");

    await click(By.xpath("//button[contains(text(), 'Migrate Repository')]"));

}

var removeRepository = async () => {
    await click(By.xpath("//a[@href='/']"));
    await click(By.xpath("//a[@href='/tester/devops-env-docker']"));
    await screen("devops-env-docker-project");

    await click(By.xpath("//a[@href='/tester/devops-env-docker/settings']"));
    await screen("open-setting");
    
    await click(By.xpath("//button[contains(text(), 'Delete This Repository')]"));
    await input(By.css("#delete-repo-modal #repo_name"), "devops-env-docker");
    await screen("confirm-remove-repo");

    await click(By.xpath("//button[contains(text(), 'Delete Repository')]"));
    await screen("removeRepository");
}

var config = async () => {
    await openWebsite();
    await changeLocale();
    
    await login();
    await changeLocale();
    
    await addRemoteRepository();

    await removeRepository();

    await screen("end");
    await sleep(3000);
};

var start = async () => {
    beforeEach();
    await config();
}

start().then(result => {
    console.log(result);
    afterEach();
}).catch(error => {
    console.log(error);
    //afterEach();
});